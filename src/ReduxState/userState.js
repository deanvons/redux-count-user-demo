// reducer to update user state

export const ACTION_SET_USER = "[user] set-user";
export const ACTION_LOAD_RANDOM_USER = "[user] load-random-user";

const initialState = {
  userName: "Some User",
  userDetails:{
      email:"default emial"}
};

export const setUserAction = (user) => {
  return { type: ACTION_SET_USER, payload: user };
};

export const loadRandomUser = () => {
    return { type: ACTION_LOAD_RANDOM_USER};
  };

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_SET_USER:
      return { ...state, userDetails: action.payload };
    default:
      return state;
  }
};

//middleware to fetch user details
export const userMiddleware = ({dispatch}) => (next) => (action) => {
  next(action);

  if (action.type === ACTION_LOAD_RANDOM_USER) {
    fetch("https://randomuser.me/api/")
      .then((response) => response.json())
      .then((result) => dispatch(setUserAction(result.results[0])));
  }
};
