export const ACTION_COUNTER_INCREMENT = "[count] increment";
export const ACTION_COUNTER_BOOST = "[count] boost";
export const ACTION_COUNTER_ASYNC = "[count] async"

// Actions
export const increment = () => {
  return { type: ACTION_COUNTER_INCREMENT };
};

export const boost = (boostValue) => {
  return { type: ACTION_COUNTER_BOOST, payload: boostValue };
};

export const lengthyAsync = () => {
    return { type: ACTION_COUNTER_ASYNC};
  };
  

// Reducer

export const counterReducer = (state = 0, action) => {
  switch (action.type) {
    case ACTION_COUNTER_INCREMENT:
      return state + 1;
    case ACTION_COUNTER_BOOST:
      return state + action.payload;
    default:
      return state;
  }
};


//middleware
export const counterMiddleware = () => next => action =>{
next(action)
    if(action.type === ACTION_COUNTER_ASYNC){
        //do some asyn code
       // if you want to use async await keyword use thunk 
       // fetch is fine in middleware
    }
    if(action.type === ACTION_COUNTER_INCREMENT){
        //do some asyn code
        
    }




}