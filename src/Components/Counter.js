import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { boost, increment, lengthyAsync } from "../ReduxState/counterState";
import { loadRandomUser } from "../ReduxState/userState";

function Counter(props) {
  const count = useSelector((state) => state.count);
  const user = useSelector((state)=>state.user.userDetails)
  const dispatch = useDispatch();

  const handleIncrement = () => {
    dispatch(increment());
  };

  const handleBoost = () => {
    dispatch(boost(87));
  };

  const handleSomeLengthyThing = () => {
    // invoke middleware
    dispatch(lengthyAsync());
  };

  const handleLoadRandomUser = () => {
      dispatch(loadRandomUser())
  };

  return (
    <div>
      <h1>Lets count</h1>
      <h4>current count is {count}</h4>
      <h4>current user email is {user.email}</h4>
      <button onClick={handleIncrement}>Increase Count by 1</button>
      <button onClick={handleBoost}>Increase Count by n</button>
      <button onClick={handleSomeLengthyThing}>
        Increase Count done by some server
      </button>
      <button onClick={handleLoadRandomUser}>Load Random User</button>
    </div>
  );
}

export default Counter;
