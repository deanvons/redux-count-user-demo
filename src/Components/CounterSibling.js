import React from "react";
import { useSelector } from "react-redux";

function CounterSibling(props) {
  const count = useSelector((state) => state.count);
  return (
    <div>
      <h1>Lets count somewhere else</h1>
      <h4>current count is {count}</h4>
    </div>
  );
}

export default CounterSibling;
