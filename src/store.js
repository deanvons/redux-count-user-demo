import { combineReducers, createStore, applyMiddleware } from "redux";
import { counterMiddleware, counterReducer } from "./ReduxState/counterState";
import { composeWithDevTools } from "redux-devtools-extension";
import { userMiddleware, userReducer } from "./ReduxState/userState";

const appReducers = combineReducers({ count: counterReducer, user:userReducer });

export default createStore(
  appReducers,
  composeWithDevTools(applyMiddleware(counterMiddleware,userMiddleware))
);
