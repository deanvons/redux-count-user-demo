import logo from './logo.svg';
import './App.css';
import Counter from './Components/Counter';
import CounterSibling from './Components/CounterSibling';

function App() {
  return (
    <div className="App">
      <Counter/>
      <CounterSibling/>
    </div>
  );
}

export default App;
